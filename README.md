# README

A [Telegram bot](https://core.telegram.org/bots) that returns current financial data, using the [Alpha Vantage API](https://www.alphavantage.co/documentation).

## App Information

App Name: go-finance-bot

Created: January-February 2021

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/go-finance-bot)

## Tech Stack

- Go
- Telegram API
- Alpha Vantage API
- Docker

## Run with Docker (development)

```
$ docker-compose up -d --build
```

Last updated: 2024-11-27
