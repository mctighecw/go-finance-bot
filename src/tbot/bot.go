package tbot

import (
  "../config"
  "../lib"

  "log"
  "regexp"
  "strings"

  "github.com/go-telegram-bot-api/telegram-bot-api"
)

func CreateBot() {
  token := config.TELEGRAM_BOT_TOKEN

  if token == "" {
    log.Fatal("No authorization token")
    return
  }

  bot, err1 := tgbotapi.NewBotAPI(token)
  lib.LogError(err1)

  log.Printf("Authorized for account %s", bot.Self.UserName)

  config := tgbotapi.UpdateConfig{}
  updates, err2 := bot.GetUpdatesChan(config)
  lib.LogError(err2)

  for update := range updates {
    var res string
    chatId := update.Message.Chat.ID
    messageText := strings.ToUpper(update.Message.Text)

    if messageText == "HELP" || messageText == "?" {
      res = HELP_MESSAGE
    } else {
      isSlash, _ := regexp.MatchString("/.", messageText)

      reCurr, _ := regexp.Compile("[A-Z]{3}-[A-Z]{3}")
      isCurr := reCurr.MatchString(messageText)

      if isSlash {
        res = getStockData(messageText[1:])
      } else if isCurr {
        res = getCurrencyExchange(messageText)
      } else {
        res = UNRECOGNIZED_COMMAND
      }
    }

    msg := tgbotapi.NewMessage(chatId, res)
    msg.ParseMode = "MarkdownV2"

    _, err4 := bot.Send(msg)
    lib.LogError(err4)
  }
}
