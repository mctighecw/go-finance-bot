package tbot

const (
  HELP_MESSAGE = "*Help*\nTo get a stock quote, send the command _/dow_\nTo get a currency exchange rate, send the command _usd\\-eur_"
  UNRECOGNIZED_COMMAND = "*Unrecognized command*\nFor help, send the message _help_ or _?_"
  STOCK_ERROR = "*Error*\nNo data for this stock, did you mistype?"
  CURRENCY_ERROR = "*Error*\nNo data for this currency exchange, did you mistype?"
)
