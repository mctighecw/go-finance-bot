package tbot

import (
  "../config"
  "../lib"

  "encoding/json"
  "fmt"
  "io/ioutil"
  "net/http"
  "strings"
)

func requestData(url string) *http.Response {
  request, err1 := http.NewRequest("GET", url, nil)
  lib.LogError(err1)

  request.Header.Set("Accept", "application/json")

  response, err2 := http.DefaultClient.Do(request)
  lib.LogError(err2)

  return response
}

func getStockData(symbol string) string {
  url := fmt.Sprintf("https://%s/query?%s&%s&%s&symbol=%s",
    config.ALPHAVANTAGE_URL, config.API_KEY,
    config.TIME_SERIES_INTRADAY, config.FIVE_MIN_INTERVAL, symbol,
  )

  response := requestData(url)

  if response.StatusCode == 200 {
    defer response.Body.Close()

    data, err1 := ioutil.ReadAll(response.Body)
    lib.LogError(err1)

    var dataResponse interface{}
    err2 := json.Unmarshal(data, &dataResponse)
    lib.LogError(err2)

    // Decode JSON response to map
    responseMap := dataResponse.(map[string]interface{})

    // Note: Alpha Vantage errors also have status 200, but with a JSON key "Error Message"
    errorMessage := responseMap["Error Message"]
    if errorMessage != nil {
      return STOCK_ERROR
    }

    timeSeriesData := responseMap["Time Series (5min)"].(map[string]interface{})

    key, value := lib.GetLastSortedItem(timeSeriesData)
    formattedResponse := formatStockResponse(symbol, key, value)

    return formattedResponse
  }

  return "Error: request failed"
}

func getCurrencyExchange(exchangeString string) string {
  split := strings.Split(exchangeString, "-")
  fromCurr := split[0]
  toCurr := split[1]

  url := fmt.Sprintf("https://%s/query?%s&%s&from_currency=%s&to_currency=%s",
    config.ALPHAVANTAGE_URL, config.API_KEY,
    config.CURRENCY_EXCHANGE_RATE, fromCurr, toCurr,
  )

  response := requestData(url)

  if response.StatusCode == 200 {
    defer response.Body.Close()

    data, err1 := ioutil.ReadAll(response.Body)
    lib.LogError(err1)

    var dataResponse interface{}
    err2 := json.Unmarshal(data, &dataResponse)
    lib.LogError(err2)

    responseMap := dataResponse.(map[string]interface{})

    errorMessage := responseMap["Error Message"]
    if errorMessage != nil {
      return CURRENCY_ERROR
    }

    currencyData := responseMap["Realtime Currency Exchange Rate"].(map[string]interface{})
    exchangRateData := currencyData["5. Exchange Rate"].(string)
    refreshedDate := currencyData["6. Last Refreshed"].(string)
    formattedResponse := formatCurrencyResponse(fromCurr, toCurr, exchangRateData, refreshedDate)

    return formattedResponse
  }

  return "Error: request failed"
}

func formatCurrencyResponse(fromCurr string, toCurr string, rate string, date string) string {
  exchangeRate := lib.FormatDecimalNumber(rate)
  dateString := lib.FormatDatestring(date)

  responseText := fmt.Sprintf("*Currency Exchange*: %s to %s\n%s\n_%s_\n",
    fromCurr, toCurr, exchangeRate, dateString)

  return responseText
}

func formatStockResponse(symbol string, date string, data interface{}) string {
  symbolData := data.(map[string]interface{})

  open := lib.FormatDecimalNumber(symbolData["1. open"])
  high := lib.FormatDecimalNumber(symbolData["2. high"])
  low := lib.FormatDecimalNumber(symbolData["3. low"])
  close := lib.FormatDecimalNumber(symbolData["4. close"])
  volume := symbolData["5. volume"].(string)
  dateString := lib.FormatDatestring(date)

  responseText := fmt.Sprintf("*Stock*: %s\n_open_: %s\n_high_: %s\n_low_: %s\n_close_: %s\n_volume_: %s\n_%s_\n",
    symbol, open, high, low, close, volume, dateString)

  return responseText
}
