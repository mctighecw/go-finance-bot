package main

import (
  "./lib"
  "./tbot"
)

func main() {
  lib.PrintMessage("Welcome to the Go Finance Bot")

  tbot.CreateBot()
}
