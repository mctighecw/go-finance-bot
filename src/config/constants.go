package config

import (
  "../lib"

  "fmt"
)

const (
  ALPHAVANTAGE_URL = "www.alphavantage.co"
  TIME_SERIES_INTRADAY = "function=TIME_SERIES_INTRADAY"
  CURRENCY_EXCHANGE_RATE = "function=CURRENCY_EXCHANGE_RATE"
  FIVE_MIN_INTERVAL = "interval=5min"
)

var (
  TELEGRAM_BOT_TOKEN string
  ALPHAVANTAGE_KEY string
  API_KEY string
)

func init() {
  TELEGRAM_BOT_TOKEN = lib.GetEnv("TELEGRAM_BOT_TOKEN", "")
  ALPHAVANTAGE_KEY = lib.GetEnv("ALPHAVANTAGE_KEY", "")
  API_KEY = fmt.Sprintf("apikey=%s", ALPHAVANTAGE_KEY)
}
