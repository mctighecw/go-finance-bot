package lib

import (
  "fmt"
  "os"
  "sort"
  "strings"
)

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func sortKeys(m map[string]interface{}) []string {
  // Sort and return keys
  keys := make([]string, 0, len(m))

  for k := range m {
    keys = append(keys, k)
  }
  sort.Strings(keys)

  return keys
}

func SortInterfaceValues(m map[string]interface{}) interface{} {
  // Sort keys and return new interface
  keys := sortKeys(m)

  res := make(map[string]interface{})
  for _, k := range keys {
    res[k] = m[k]
  }
  return res
}

func GetFirstSortedItem(m map[string]interface{}) (string, interface{}) {
  // Return first sorted key and value
  sortedkeys := sortKeys(m)
  k := sortedkeys[0]
  val := m[k]

  return k, val
}

func GetLastSortedItem(m map[string]interface{}) (string, interface{}) {
  // Return last sorted key and value
  sortedkeys := sortKeys(m)
  l := len(m) - 1
  k := sortedkeys[l]
  val := m[k]

  return k, val
}

func FormatDecimalNumber(m interface{}) string {
  // Escape . character to avoid markdown formatting error
  splits := strings.Split(m.(string), ".")
  val := splits[0]

  if len(splits) > 1 {
    // Take last three decimal places
    decValue := splits[1][0:3]
    val += "\\." + decValue
  }

  return val
}

func FormatDatestring(m interface{}) string {
  // Format datestring in raw format "2021-02-11 12:30:00"
  d := m.(string)
  split1 := strings.Split(d, " ")
  split2 := strings.Split(split1[0], "-")
  split3 := strings.Split(split1[1], ":")
  date := split2[2] + "/" + split2[1] + "/" + split2[0]
  time := split3[0] + ":" + split3[1]
  res := date + ", " + time

  return res
}
